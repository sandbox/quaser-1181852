#! /usr/bin/perl

use HTTP::DAV;
use Term::ANSIColor;
use Term::ANSIColor qw(:constants);
use Time::HiRes qw(gettimeofday tv_interval);

$dav = new HTTP::DAV;
$dav->DebugLevel(0);

$host       = $ARGV[0];
$user       = $ARGV[1];
$password   = $ARGV[2];
$story_name = $ARGV[3];

$url = $host . "/webdav/nodes/";

$dav->credentials( -url => $url, -user => $user, -pass => $password );

sub timming_start {
	$t0 = [gettimeofday];
}

sub result_tag {
	$tag     = shift;
	$timming = tv_interval($t0);
	$global_timing+=$timming;
	return $tag . "," . $timming . " s";
}

sub start {
	my $test = shift;
	if ( !defined $first_done ) {
		$first_done = TRUE;
	}
	else {
		success();
	}
	print WHITE. $test . " .... ";
	timming_start();
}

sub stop() {
	success();
	print "Total time : ".$global_timing." s\n";
}

sub fail {
	my $reason = shift;
	print "[" . RED . result_tag("FAILED") . WHITE . "]\n";
	die($reason);
}

sub success {
	print "[" . GREEN . result_tag("OK") . WHITE . "]\n";
}

sub check_get {
	$file    = shift;
	$pattern = shift;
	$dav->get( $file, \$file_contents )
	  or fail( "Unable to read $file: " . $dav->message . "\n" );
	@lines = split( /\r\n/, $file_contents );
	$found = 0;

	foreach $line (@lines) {
		if ( $line =~ /$pattern/ ) {
			$found = 1;
		}
	}
	if ( $found == 0 ) {
		fail( "Wrong content for $file :\n" . $file_contents . "\n" );
	}
}

sub check_binary_get {
	open( FILE, "<binary.dat" );

	binmode(FILE);

	read( FILE, $buffer, 20, 0 );

	close(FILE);

	foreach ( split( //, $buffer ) ) {

		printf( "%02x ", ord($_) );

		print "\n" if $_ eq "\n";

	}

}

sub file_exists {
	$file = shift;
	if ( $r = $dav->propfind( -url => ".", -depth => 1 ) ) {
		if ( !defined $r->get_resourcelist() ) {
			return 0;
		}
		foreach $resource ( $r->get_resourcelist()->get_resources() ) {
			if ( $resource->get_property('rel_uri') eq $file . "/" ) {
				return 1;
			}
		}
	}
	else {
		fail("Unable to make a PROPFIND");
	}
	return 0;
}

# ############################################
start("cd nodes");
$dav->open( -url => $url )
  or fail( "Unable to open connection to $url: " . $dav->message . "\n" );

# ############################################
start("Create two stories");
$node_title     = "Artisan Numérique - 1";  # perldav don't like "," in path...
$node_title_bis = "Artisan Numérique - 2";  # perldav don't like "," in path...
$url = $host . "/webdav/nodes/" . $story_name . "/";
$dav->open( -url => $url )
  or fail( "Unable to open connection to $url: " . $dav->message . "\n" );
$dav->mkcol($node_title)
  or fail( "Unable to create collection $node_title: " . $dav->message . "\n" );
$dav->mkcol($node_title_bis)
  or fail( "Unable to create collection $node_title: " . $dav->message . "\n" );

# #############################################
start("Get content.html");
$url .= $node_title . "/";
$dav->open( -url => $url )
  or fail( "Unable to open connection to $url: " . $dav->message . "\n" );
check_get( "content.html", "Created by Drupal DAV module" );

# #############################################
start("Put content.html");
$content = "Modified for testing purpose\n";
$dav->put( -local => \$content, -url => "content.html" )
  or fail( "Unable to write content.html: " . $dav->message . "\n" );
check_get( "content.html", $content );

# #############################################
start("Write an attachement");
$url =
  $host . "/webdav/nodes/" . $story_name . "/" . $node_title . "/attachements/";
$dav->open( -url => $url )
  or fail( "Unable to open connection to $url: " . $dav->message . "\n" );
$content = "This is an attachement éàö\n";
$dav->put( -local => \$content, -url => "attachement.txt" )
  or fail( "Unable to write attachement.txt: " . $dav->message . "\n" );
check_get( "attachement.txt", $content );

# #############################################
start("Rename an attachement");
$dav->move( "attachement.txt", "text_file.txt" )
  or fail( "Unable to rename attachement.txt: " . $dav->message . "\n" );
if ( file_exists("attachement.txt") ) {
	fail("Old file is still there...");
}
check_get( "text_file.txt", $content );

# #############################################
start("Copy attachement from one story to an other");
$target = "../../" . $node_title_bis . "/attachements/attachement.txt";
$dav->copy( "text_file.txt", $target )
  or fail( "Unable to copy to attachement.txt: " . $dav->message . "\n" );
check_get( "text_file.txt", $content );
check_get( $target,         $content );

# #############################################
start("Move attachement from one story to an other");
$target = "../../" . $node_title_bis . "/attachements/attachement_1.txt";
$dav->move( "text_file.txt", $target )
  or fail( "Unable to copy to attachement.txt: " . $dav->message . "\n" );
if ( file_exists("text_file.txt") ) {
	fail("Old file still exists..");
}
check_get( $target, $content );

# #############################################
start("Rename a story");
$url = $host . "/webdav/nodes/" . $story_name . "/";
$dav->open( -url => $url )
  or fail( "Unable to open connection to $url: " . $dav->message . "\n" );
$dav->move( $node_title, "Renamed Node" )
  or fail( "Unable to rename node: " . $dav->message . "\n" );
if ( file_exists($node_title) ) {
	fail("Old node still exists..");
}
$node_title = "Renamed Node";
if ( !file_exists($node_title) ) {
	fail("Renamed node doesn't exists..");
}

# #############################################
start("Delete stories");
$url = $host . "/webdav/nodes/" . $story_name . "/";
$dav->open( -url => $url )
  or fail( "Unable to open connection to $url: " . $dav->message . "\n" );
$dav->delete($node_title)
  or fail( "Unable to delete $node_title :" . $dav->message . "\n" );
$dav->delete($node_title_bis)
  or fail( "Unable to delete $node_title_bis :" . $dav->message . "\n" );
if ( file_exists($node_title_bis) || file_exists($node_title_bis) ) {
	fail("Old stories still there...");
}

stop();
