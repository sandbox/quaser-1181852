<?php

/* This file is part of "WebDav for Drupal Module".
 *    Copyright 2009, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@bad_arnumeral.fr (remove bad_ before sending an email)
 *    Site   : http://www.arnumeral.fr/node/5
 *
 * "WebDav for Drupal Module" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "WebDav for Drupal Module" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "Broken Anchor for Node comments Module"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */


function webdav_attachement_admin_settings() {
  $form=array();
  $form['webdav_attachement_update_settings'] = array (
    '#type' => 'fieldset',
    '#title' => t('Update settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['webdav_attachement_update_settings']['webdav_new_revision_for_delete_file'] = array (
      '#type' => 'checkbox', 
      '#title' => t("Add new node revision for delete attache file. Physically file not deleted"), 
      '#default_value' => variable_get("webdav_new_revision_for_delete_file", true),
      '#description' => t("Recomended check it"),
  );

  $form['webdav_attachement_update_settings']['webdav_new_revision_for_update_file'] = array (
      '#type' => 'checkbox', 
      '#title' => t("Add new node revision for update attache files."), 
      '#default_value' => variable_get("webdav_new_revision_for_update_file", true),
      '#description' => t("If you check it, list revison may be very large"),
  );
  $form['webdav_attachement_update_settings']['webdav_new_revision_for_add_file'] = array (
      '#type' => 'checkbox', 
      '#title' => t("Add new node revision for add attache files."), 
      '#default_value' => variable_get("webdav_new_revision_for_add_file", false),
      '#description' => t("Not recomended check it, list revison may be very large"),
  );

  $form['webdav_attachement_mapping_settings'] = array (
    '#type' => 'fieldset',
    '#title' => t('Mapping settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['webdav_attachement_mapping_settings']['webdav_show_image_field_file'] = array (
      '#type' => 'checkbox', 
      '#title' => t("Show image field  in webdav"), 
      '#default_value' => variable_get("webdav_show_image_field_file", true),
      //'#description' => t("If you check it, list revison may be very large"),
  );
  $form['webdav_attachement_og_settings'] = array (
    '#type' => 'fieldset',
    '#title' => t('Organics group settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['webdav_attachement_og_settings']['webdav_og_rules'] = array (
      '#type' => 'checkbox', 
      '#title' => t("Enable Organics group field permission settings in webdav"), 
      '#default_value' => variable_get("webdav_og_rules", false),
      //'#description' => t(""),
  );

  return system_settings_form($form);
}
