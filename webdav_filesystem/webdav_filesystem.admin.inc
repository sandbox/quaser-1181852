<?php

/* This file is part of "WebDav for Drupal Module".
 *    Copyright 2009, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@bad_arnumeral.fr (remove bad_ before sending an email)
 *    Site   : http://www.arnumeral.fr/node/5
 *
 * "WebDav for Drupal Module" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "WebDav for Drupal Module" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "Broken Anchor for Node comments Module"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
function webdav_filesystem_admin_settings() {
  $form=array();
  $filesystems = webdav_filesystem_load_all();
  $rows=array();
  //  $header = array(t('Name'), t('Operations'));
  $header = array(
    'name' => array('data' => t('Name'), 'field' => 'u.name'),
    'operations' => array('data' => t('Operations')),
  );

  $options = array();
  foreach ($filesystems as $filesystem) {
    $options[] = array(
      'name' => $filesystem['name'],
      'operations' => l(t('Edit'), 'admin/settings/webdav/filesystem/edit/'. $filesystem['fid']) . ' · ' . l(t('Delete'), 'admin/settings/webdav/filesystem/delete/'. $filesystem['fid']),
    );
  }
  
 $form['list'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' =>$options,
    '#empty' => t('No filesystem defined, please add one'),
  );

  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Add a new FileSystem'), 
    '#submit' => array('webdav_filesystem_add'),
  );

  return system_settings_form($form);
}

function webdav_filesystem_form_validate($form, &$form_state) {
  $values=$form_state['values'];
  if (trim($values['name']) == '') {
    form_set_error('name', t('Please enter an filesystem name.'));
  }

  if (trim($values['path']) == '') {
    form_set_error('path', t('Please enter a filesystem URL.'));
  }

  return !form_get_errors();
}

function webdav_filesystem_add($form, &$form_state) {
  drupal_goto("admin/settings/webdav/filesystem/add");
}


function webdav_filesystem_form1 ($whatever, $fid='add') {
  if ($fid!='add') {
   $filesystem=webdav_filesystem_load1($fid);
   dvm($filesystem,'asdfasdf 444 filesystem');
   dvm( $whatever,'asdfasdf 333 filesystem');
    $form['fid'] = array(
    '#type' => 'hidden', 
    '#value' => $fid, 
  );
  } else {$filesystem=array();$filesystem['name']='';$filesystem['path']=''; }
  $form['name'] = array(
    '#title' => t('Name'),
    '#description' => t('File system name. It will be used as webdav base url so be carefull with extended characters...<bR>
    This will also be used as permission.'),
    '#type' => 'textfield', 
    '#default_value' => $filesystem['name'], 
  );
  $form['path'] = array(
    '#title' => t('Path'),
    '#description' => t('Real file system location. Apache must have all permission on this.'),
    '#type' => 'textfield', 
    '#default_value' => $filesystem['path'], 
  );
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Submit'), 
  );
  return $form;
}

function webdav_filesystem_form($whatever,$fid='add') {
  if (($fid!='add')and !empty($fid['build_info']['args']) ) {

    $fid0 = $fid['build_info']['args'];
    foreach  ($fid0 as $fid00){$fid1=$fid00;}
    $fid1=$fid0[0];
    $filesystem=webdav_filesystem_load1($fid1);
    $form['fid'] = array(
    '#type' => 'hidden', 
    '#value' => $fid1, 
  );
  } else {$filesystem=array();$filesystem['name']='';$filesystem['path']=''; }
  $form['name'] = array(
    '#title' => t('Name'),
    '#description' => t('File system name. It will be used as webdav base url so be carefull with extended characters...<bR>
    This will also be used as permission.'),
    '#type' => 'textfield', 
    '#default_value' => $filesystem['name'], 
  );
  $form['path'] = array(
    '#title' => t('Path'),
    '#description' => t('Real file system location. Apache must have all permission on this.'),
    '#type' => 'textfield', 
    '#default_value' => $filesystem['path'], 
  );
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Submit'), 
  );
  return $form;
}

function webdav_filesystem_form_submit($form, &$form_state) {

  if (!empty($form_state['build_info']['args'])) {
    error_log("here");
    $values=$form_state['values'];
    drupal_write_record('webdav_filesystems', $values, 'fid');
  } else {
    $values=$form_state['values'];
    drupal_write_record('webdav_filesystems', $values);
  }

  drupal_goto('admin/settings/webdav/filesystem');
}

function webdav_filesystem_delete($fid) {
  db_query("DELETE FROM {webdav_filesystems} WHERE fid=:fid", array(':fid' => $fid));
  $message = t('Filesystem deleted.');
  drupal_set_message($message);
  drupal_goto('admin/settings/webdav/filesystem');
}