<?php
/* This file is part of "WebDav for Drupal Module".
 *    Copyright 2009, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@bad_arnumeral.fr (remove bad_ before sending an email)
 *    Site   : http://www.arnumeral.fr/node/5
 *
 * "WebDav for Drupal Module" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "WebDav for Drupal Module" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "Broken Anchor for Node comments Module"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */



/**
 * WebDAV server configuration form.
 *
 * @return form
 */
function webdav_admin_settings() {
  $form=array();
  $form['webdav_performances'] = array (
    '#type' => 'fieldset',
    '#title' => t('Performances'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['webdav_performances']['information'] = array(
    '#prefix' => "<div>
    In order to optimize performances :
    <ul>
       <li>Think about removing 'statistics' module if you don't need it...
    </ul>
    ",
    '#suffix' => '</div>',
  );
  $form['webdav_performances']['webdav_cache_routes'] = array (
      '#type' => 'checkbox', 
      '#title' => t("Routes caching"), 
      '#default_value' => webdav_cache_routes(), 
      '#description' => t("Cache routes (experimental)")
  );
  $form['webdav_performances']['webdav_cache_members'] = array (
      '#type' => 'checkbox', 
      '#title' => t("Members caching"), 
      '#default_value' => webdav_cache_members(), 
      '#description' => t("Cache members (experimental)")
  );
  $form['webdav_debugging'] = array (
    '#type' => 'fieldset',
    '#title' => t('Debugging'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['webdav_debugging']['webdav_watch_dog'] = array (
      '#type' => 'checkbox', 
      '#title' => t("Use WatchDog"), 
      '#default_value' => webdav_watch_dog(), 
      '#description' => t("Log every user access to watchdog. This will reduce performances !!")
  );

  $form['webdav_debugging']['webdav_debug_level'] = array (
        '#title' => t("Debug level"),
        '#description' => t("Set the debugging output level. Every logging will use php error_log function. On UNIX system, this will end up in apache error logs."),
        '#type' => 'select',
        '#default_value' => webdav_debug_level(), 
        '#options' => array (
  0 => t('Disabled'),
  1 => t('Enabled'),
  2 => t('Verbose'),
  3 => t('Full'),
  )
  );

  $form['webdav_http']= array (
    '#type' => 'fieldset',
    '#title' => t('HTTP settings'),
    '#weight' => -3,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['webdav_http']['webdav_http_compression'] = array (
      '#type' => 'checkbox', 
      '#title' => t("Compression"), 
      '#default_value' => webdav_http_compression(), 
      '#description' => t("Activate WebDAV HTTP compression for text contents.")
  );


  $form['webdav_dav']= array (
    '#type' => 'fieldset',
    '#title' => t('DAV settings'),
    '#weight' => -1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['webdav_dav']['webdav_lock_timeout'] = array (
      '#type' => 'textfield', 
      '#title' => t("Locks timeout"), 
      '#default_value' => webdav_lock_timeout(), 
      '#description' => t("Delay before a lock is automaticaly released (seconds).")
  );

  $form['webdav_advanced'] = array (
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  $form['webdav_advanced']['webdav_redirection_mode'] = array (
      '#type' => 'select', 
      '#title' => t("Redirect wrong URL"),
      '#options' => array (
  0 => t('Yes'),
  1 => t('No'),
  2 => t('Automatic'),
  ),
      '#default_value' => webdav_redirection_mode(), 
      '#description' => t("
      Redirect WebDAV client when the URL don't end with / for a collection.")
  );

  return system_settings_form($form);
}