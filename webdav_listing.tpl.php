<?php

/* This file is part of "WebDav for Drupal Module".
 *    Copyright 2009, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@bad_arnumeral.fr (remove bad_ before sending an email)
 *    Site   : http://www.arnumeral.fr/node/5
 *
 * "WebDav for Drupal Module" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "WebDav for Drupal Module" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "Broken Anchor for Node comments Module"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

global $language;

$locale=$language->prefix;
$site=variable_get('site_name', 'Drupal site');

$title=t("@site - '@folder' WebDAV folder...", array(
  '@site'=>$site,
  '@folder'=>($root['name']==''?t("WebDAV Root"):$root['name']),
));
?>
<h2><?php print $title ?></h2>
<div class='webdav-listing'>
<table>
	<tr>
		<th class='icon' width="22px">&nbsp;</th>
		<th class='filename'><?php print  t("Filename")?></th>
		<th><?php print  t("type")?></th>
		<th><?php print  t("Size")?></th>
		<th><?php print  t("Created")?></th>
		<th><?php print  t("Modified")?></th>
	</tr>
	<tr>
		<td><img
			src="<?php print base_path().drupal_get_path('module', 'webdav')."/images/up.png" ?>" /></td>
		<td><a href=".."><?php print  $root['name']==''?t('Return to @site', array('@site'=>$site)):t('Parent folder') ?></a></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<?php
	$count=0;
	if(isset($items)){ //i
	foreach ($items as $_item) {
	  $type=drupal_get_path('module', 'webdav')."/images/mimetypes/".str_replace("/", "-", $_item['type']).".png";
	  if (!file_exists($type)) {
	    $ipos=strpos($_item['type'], "/");
	    if ($ipos!==false) {
	      $type=drupal_get_path('module', 'webdav')."/images/mimetypes/".substr($_item['type'], 0, $ipos).".png";
	    }
	  } 
	  if (!file_exists($type)) {
	    $type=drupal_get_path('module', 'webdav')."/images/mimetypes/unknown.png";
	  }
	  ?>
	<tr <?php print $count%2==0?"class='a'":""?>>
		<td><img src="<?php print base_path().$type ?>" /></td>
		<td><a href="<?php print $_item['id']?>"><?php print  $_item['name'] ?></a></td>
		<td><?php print  $_item['type']=='collection'?'':$_item['type'] ?></td>

		<td><?php if(isset($_item['size'])) {print  $_item['size']; }	?></td>
		<td><?php if(isset($_item['created'])) {print  format_date($_item['created'],'small');} ?></td>
		<td><?php if(isset($_item['changed'])) {print  format_date($_item['changed'],'small');} ?></td>
	</tr>
	<?php
	$count++;
	}
	} //i
	?>
</table>


</div>
<!-- Quaser edition :-) -->
<div class='powered-by'>
Powered by <a href="http://drupal.org/projects/webdav" target="_blank">WebDAV Server for Drupal</a> Quaser edition
</div>