<?php

/* This file is part of "WebDav for Drupal Module".
 *    Copyright 2009, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@bad_arnumeral.fr (remove bad_ before sending an email)
 *    Site   : http://www.arnumeral.fr/node/5
 *
 * "WebDav for Drupal Module" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "WebDav for Drupal Module" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "Broken Anchor for Node comments Module"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */


function webdav_node_admin_settings() {
  $form['node_created'] = array (
    '#type' => 'fieldset',
    '#title' => t('When a node is created'),
    '#weight' => -2,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['node_created']['webdav_node_create_node_default_body'] = array (
      '#type' => 'textarea', 
      '#title' => t("Default node body"), 
      '#default_value' => webdav_node_create_node_default_body(), 
      '#description' => t("This is the default node body used when creating a new node with WebDAV.")
  );
  $form['node_created']['webdav_node_create_node_default_log_message'] = array (
      '#type' => 'textfield', 
      '#title' => t("Default log message"), 
      '#default_value' => webdav_node_create_node_default_log_message(), 
      '#description' => t("Default log message used when creating a new node with WebDAV.")
  );
  $form['node_created']['webdav_node_create_node_not_published'] = array (
      '#type' => 'checkbox', 
      '#title' => t("Prevent publish"), 
      '#default_value' => webdav_node_create_node_not_published(), 
      '#description' => t("Prevent nodes to be publish when created by webdav AND default setting for new node is published.")
  );
  

  $form['node_updated'] = array (
    '#type' => 'fieldset',
    '#title' => t('When a node is updated'),
    '#weight' => -1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['node_updated']['webdav_node_update_node_create_revision'] = array (
      '#type' => 'checkbox', 
      '#title' => t("Create a revision"), 
      '#default_value' => webdav_node_update_node_create_revision(), 
      '#description' => t("Create a new revision each time a node is saved with WebDAV.")
  );

  $form['node_updated']['webdav_node_update_default_revision_message'] = array (
      '#type' => 'textfield', 
      '#title' => t("Revision message"), 
      '#default_value' => webdav_node_update_default_revision_message(), 
      '#description' => t("Message to use as revision log.")
  );



  $form['node_browsing'] = array (
    '#type' => 'fieldset',
    '#title' => t('When browsing nodes'),
    '#weight' => -3,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['node_browsing']['webdav_node_node_browsing_limit'] = array (
      '#type' => 'textfield', 
      '#size' => 6,
      '#title' => t("Number of nodes limitation"), 
      '#default_value' => webdav_node_node_browsing_limit(), 
      '#description' => t("Enter a limit to the number of node to put in 'nodes' folder. 0 or less means infinite.")
  );

  $formats = filter_formats();
  foreach ($formats as $name => $format) {
    $form['mapping_'.$format->format] = array (
    '#type' => 'fieldset',
    '#title' => t("Node content file parameters from when input format is '@format_name'", array ('@format_name' => $format->name)),
    '#weight' => -20,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
    );
    $form['mapping_'.$format->format]["webdav_node_extension_for_input_format_".$format->format] = array (
      '#type' => 'textfield', 
      '#title' => 'File extension',
      '#default_value' => webdav_node_extension_for_input_format($format->format),
      '#size' => 6, 
      '#maxlength' => 6, 
      '#description' => t("Extension to use when the node input format is '@format_name'", array ('@format_name' => $format->name)), 
    );

    $variable = 'dav_nodes_mime_type_mapping_' . $format->format;
    $form['mapping_'.$format->format]["webdav_node_mime_type_for_input_format_".$format->format] = array (
      '#type' => 'textfield',
      '#title' => 'Mime type',
      '#default_value' => webdav_node_mime_type_for_input_format($variable, 'text/html'), 
      '#size' => 15, 
      '#maxlength' => 15, 
      '#description' => t("Mime Type to use when the node input format is '@format_name'", array ('@format_name' => $format->name)), 
    );
  }

  $form['node_advanced'] = array (
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  $form['node_advanced']['webdav_displayname_mode'] = array (
      '#type' => 'select', 
      '#title' => t("Use display names"),
      '#options' => array (
  0 => t('Yes'),
  1 => t('No'),
  2 => t('Automatic'),
  ),
      '#default_value' => webdav_displayname_mode(), 
      '#description' => t("
      When using display names, the filename is different of the real name. 
      This allows to have full character range in a node title usable in any 
      WebDav browser. Problem is WebDAV client can get lost for operations 
      like renaming, copying, etc. So if you just use this module 
      to browse your nodes and edit contents, you can safely activate 
      this to have a better presentation.<br>
      In automatic mode, the module will decide to use this or not depends of
      the connected WebDAV client.")
  );
  return system_settings_form($form);
}
