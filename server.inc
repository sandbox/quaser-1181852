<?php

/* This file is part of "WebDav for Drupal Module".
 *    Copyright 2009, arNuméral
 *    Author : Yoran Brault
 *    eMail  : yoran.brault@bad_arnumeral.fr (remove bad_ before sending an email)
 *    Site   : http://www.arnumeral.fr/node/5
 *
 * "WebDav for Drupal Module" is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * "WebDav for Drupal Module" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with "Broken Anchor for Node comments Module"; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */


require_once 'HTTP/WebDAV/Server.php';

class DrupalDavServer extends HTTP_WebDAV_Server {
  var $dav_powered_by = 'Drupal WebDAV (+http://drupal.org/project/webdav)';

  /** Serve a webdav request. */
  function ServeRequest() {
    parent :: ServeRequest();
  }

  function checkLock ($path) {
    //    $cursor = db_query("SELECT * FROM {webdav_locks} WHERE path = '%s'",$path);
    //    $lock = db_fetch_array($cursor);
    //    if (webdav_is_debug()) webdav_debug("Checking $path lock... : ".$lock['token']);
    //    return $lock;
    return NULL;
  }

  /** PROPFIND method handler. */
  function PROPFIND(& $options, & $files) {
    webdav_session_start($options['path']);
    if (webdav_session_failed()) return webdav_session_close();


    // search for the associated collection
    $handler = webdav_route_resolve(webdav_session('route'));
    if (!webdav_check_collection_perms($handler)) {
      return webdav_session_close();
    }
    $resource  = webdav_resource($handler);
    if ($resource===false) {
      webdav_session_close(WEBDAV_STATUS_NOT_FOUND);
      return false;
    }

    // if client forget the leading / for a collection URL, we should redirect it
    webdav_redirect_wrong_collection( $resource);

    /*  first, this is a folder...
     *  FIXME sure this will not work for depth > 1
     */
    $members=array();
    if ($resource['type'] == 'collection' && $options['depth'] != 0) {
      $members = webdav_members($handler);
    }

    // make the root item first of the list
    array_unshift($members, $resource);

    // Convert items to DAV files
    $files['files'] = array ();
    $members[0]['id']=''; //
    foreach ($members as $_item) {
      $files['files'][] = $this->convert_item($session, $_item);
    }

    // DEBUG: display result
    if (webdav_is_debug()) {
      $count=0;
      foreach ($files['files'] as $file) {
        webdav_debug('  + ' . $file['path']);
        $count++;
        if ($count > 5) {
          break;
        }
      }
    }

    webdav_session_close();
    return true; // No status for this, everything is handled by undelying class
  }

  /** convert DFS item to Dav item */
  function convert_item(&$session, &$resource) {
    $file = array ();

    // Common properties
    $route=webdav_session('route');
    array_push($route, $resource['id']);
    $route[0]=WEBDAV_ROOT_MENU;
    $file['path'] = base_path() . webdav_route_to_path($route);

    $file['props'] = array ();

    if (!empty($resource['name'])) {
      $file['props'][] = $this->mkprop(
    'displayname',$resource['name']); // No need to convert HTML entities (< > &) as is done by pear
    }
     
    if (!empty($resource['created'])) {
      $file['props'][] = $this->mkprop('creationdate', $resource['created']);
    }
    if (!empty($resource['changed'])) {
      $file['props'][] = $this->mkprop('getlastmodified', $resource['changed']);
    }

    if ($resource['type'] == 'collection') {
      $file['path'] = webdav_slashify($file['path']);
      $file['props'][] = $this->mkprop('resourcetype', 'collection');
      $file['props'][] = $this->mkprop('getcontentlength', 1);
    } else {
      $file['path'] = webdav_unslashify($file['path']);
      $file['props'][] = $this->mkprop('creationdate', $resource['created']);
      $file['props'][] = $this->mkprop('getlastmodified', $resource['changed']);
      $file['props'][] = $this->mkprop('resourcetype', '');
      $file['props'][] = $this->mkprop('getcontenttype', $resource['type']);
      $file['props'][] = $this->mkprop('getcontentlength', $resource['size']);
    }
    return $file;
  }

  /**
   * GET method handler
   *
   * @param  array  parameter passing array
   * @return bool   true on success
   */
  function GET(& $options) {
    webdav_session_start($options['path']);

    // search for the associated collection
    $handler   = webdav_route_resolve(webdav_session('route'));
    webdav_check_collection_perms($handler); // FIXME : we don't have to give the not found information
    $resource  = webdav_resource($handler);
    if ($resource===false) {
      return webdav_session_close(WEBDAV_STATUS_NOT_FOUND);
    }
    if (webdav_session_failed()) {
      if ($resource['type']=='collection') {
        webdav_set_output_type(WEBDAV_CONTENT_DRUPAL);
      }
      return webdav_session_close();
    }

    if ($resource['type']!='collection') {

      $options['mimetype'] = $resource['type'];
      if (!empty($resource['encoding'])) {
        $options['mimetype'].='; charset=' . $resource['encoding'];
      } else {
        // FIXME No encoding <=> binary, not that clever, we need a proper contet-type handling here
        webdav_set_output_type(WEBDAV_CONTENT_BINARY);
      }
      if (webdav_is_debug()) webdav_debug("Document encoding : ".$options['mimetype']);
      $options['mtime'] = $resource['changed'];
      $options['ctime'] = $resource['created'];
      $options['size'] = $resource['size'];

      // Load string content
      webdav_session_set_status(WEBDAV_STATUS_OK);
      $result=webdav_operation($handler, 'get');
      if (webdav_session_failed()) return webdav_session_close();

      if (is_string($result)) {
        // Save temporary file
        $file_name = tempnam(file_directory_temp(), 'FOO');
        $handle = fopen($file_name, 'w');
        fwrite($handle, $result);
        fclose($handle);
        $stream=fopen($file_name, 'r');
      } else {
        $stream=$result;
      }
      $options['stream'] = $stream;
      webdav_session_close();
      return true;
    } else {
      webdav_redirect_wrong_collection( $resource);

      if (empty($resource['name'])) {
        $resource['name']=$resource['id'];
      }
      $members = webdav_members($handler);
      if(isset($members)) { //i
      foreach ($members as &$_item) {
        //if(isset($_item['name'])) { //i

        if (empty($_item['name'])) {
          $_item['name']=$_item['id'];
        }
	//} //i
        if ($_item['type']=='collection') {
          $_item['id']=webdav_slashify($_item['id']);
        }
      }
      } //i
      $contents = theme('webdav_listing', array( 'root' => $resource,'items' =>  $members)); //i
//i      $contents = theme('webdav_listing', $resource, $members);

      $path = drupal_get_path('module', 'webdav');
      drupal_add_css($path . '/webdav_listing.css');
      webdav_set_output_type(WEBDAV_CONTENT_DRUPAL);
      print $contents;
      webdav_session_close();
      return false;
    }
  }

  /**
   * PUT method handler
   *
   * @param  array  parameter passing array
   * @return bool   true on success
   */
  function PUT(& $options) {
    webdav_session_start($options['path']);
    if (webdav_session_failed()) return webdav_session_close();

    // lookup item (if it is an existing one)
    $route=webdav_session('route');
    $name=array_pop($route);
    $handler = webdav_route_resolve($route);
    if (!webdav_check_collection_perms($handler)) {
      return webdav_session_close();
    }
    $parent_item  = webdav_resource($handler);
    if ($parent_item===false) {
      return webdav_session_close(WEBDAV_STATUS_NOT_FOUND);
    }

    // prepare item stuff
    $file_name = file_directory_temp()."/".$name;
    $handle = fopen($file_name, 'w');
    while (!feof($options["stream"])) {
      $data= fread($options["stream"], 8192);
      fwrite($handle, $data);
    }
    fclose($handle);

    webdav_session_set_status(WEBDAV_STATUS_CREATED);
    webdav_operation($handler, 'put', $file_name);
    return webdav_session_close();
  }

  /**
   * MKCOL method handler
   *
   * @param  array  general parameter passing array
   * @return bool   true on success
   */
  function MKCOL($options) {
    webdav_session_start($options['path']);
    if (webdav_session_failed()) return webdav_session_close();

    // lookup item
    $route=webdav_session('route');
    $new_folder=array_pop($route);
    $handler   = webdav_route_resolve($route);
    if (!webdav_check_collection_perms($handler)) return webdav_session_close();

    $resource  = webdav_resource($handler);
    if ($resource===false) return webdav_session_close(WEBDAV_STATUS_NOT_FOUND);

    webdav_session_set_status(WEBDAV_STATUS_CREATED);
    webdav_operation($handler, 'create', $new_folder);
    return webdav_session_close();
  }

  /**
   * DELETE method handler
   *
   * @param  array  general parameter passing array
   * @return bool   true on success
   */
  function DELETE($options) {
    webdav_session_start($options['path']);
    if (webdav_session_failed()) return webdav_session_close();

    // lookup item
    $handler   = webdav_route_resolve(webdav_session('route'));
    if (!webdav_check_collection_perms($handler)) return webdav_session_close();
    $resource  = webdav_resource($handler);
    if ($resource===false) {
      return webdav_session_close(WEBDAV_STATUS_NOT_FOUND);
    }


    // prepare item stuff
    webdav_session_set_status(WEBDAV_STATUS_NO_CONTENT);
    webdav_operation($handler, 'delete');
    // cleaning locks
    if (!webdav_session_failed()) {
     // db_query("DELETE FROM {webdav_locks} WHERE path='%s'", $options['path']);
      db_delete('webdav_locks')->condition('path', $options['path'])->execute();
    }
    cache_clear_all('w');
    return webdav_session_close();
  }

  /**
   * OPTIONS method handler
   *
   * @param  array  general parameter passing array
   * @return bool   true on success
   */
  function OPTIONS($options) {
    webdav_session_start($options['path']);
    if (webdav_session_failed()) return webdav_session_close();

    // lookup item
    $handler   = webdav_route_resolve(webdav_session('route'));
    //        if ( (   !(($this->_SERVER['REQUEST_METHOD'] == 'OPTIONS') && ($this->path == "/")))
    if (!webdav_check_collection_perms($handler)) return webdav_session_close();
    $resource  = webdav_resource($handler);
    if ($resource===false) {
      return webdav_session_close(WEBDAV_STATUS_NOT_FOUND);
    }

    // if client forget the leading / for a collection URL, we should redirect it
    webdav_redirect_wrong_collection( $resource);

    $resource=webdav_resource($handler);
    header("Content-Type:".$resource['type']=='collection'?'httpd/unix-directory':$resource['type']);
    webdav_session_close(WEBDAV_STATUS_OK); // We
  }


  /**
   * MOVE method handler
   *
   * @param  array  general parameter passing array
   * @return bool   true on success
   */
  function MOVE($options) {
    webdav_session_start($options['path']);
    if (webdav_session_failed()) return webdav_session_close();

    // lookup item
    $arguments=webdav_session('route');
    $source_collection   = webdav_route_resolve($arguments);
    if (!webdav_check_collection_perms($source_collection)) return webdav_session_close();
    $source_item  = webdav_resource($source_collection);
    if ($source_item===false) {
      return webdav_session_close(WEBDAV_STATUS_NOT_FOUND);
    }

    $target_session=webdav_session_create($options['dest']);
    $target_name=array_pop($target_session->route);
    $target_collection   = webdav_route_resolve($target_session->route);
    if (!webdav_check_collection_perms($target_collection)) return webdav_session_close();
    $target_item  = webdav_resource($target_collection);
    if ($target_item===false) {
      return webdav_session_close(WEBDAV_STATUS_NOT_FOUND);
    }

    webdav_session_set_status(WEBDAV_STATUS_CREATED);
    webdav_operation($target_collection, 'move',  $target_name, $source_collection);
    if (!webdav_session_failed()) {
      // moving locks
//i      db_query("update {webdav_locks} set path='%s' where path='%s'",$options['dest'],$options['path']);
db_update('webdav_locks') ->fields(array( 'path' => $options['dest'],  ))->condition('path', $options['path'] ) ->execute();
    }
    return webdav_session_close();
  }


  /**
   * COPY method handler
   *
   * @param  array  general parameter passing array
   * @return bool   true on success
   */
  function COPY($options) {
    webdav_session_start($options['path']);
    if (webdav_session_failed()) return webdav_session_close();

    // lookup item
    $arguments=webdav_session('route');
    $source_collection   = webdav_route_resolve($arguments);
    if (!webdav_check_collection_perms($source_collection)) return webdav_session_close();
    $source_item  = webdav_resource($source_collection);
    if ($source_item===false) {
      return webdav_session_close(WEBDAV_STATUS_NOT_FOUND);
    }

    $target_session=webdav_session_create($options['dest']);
    $target_name=array_pop($target_session->route);
    $target_collection   = webdav_route_resolve($target_session->route);
    if (!webdav_check_collection_perms($target_collection)) return webdav_session_close();
    $target_item  = webdav_resource($target_collection);
    if ($target_item===false) {
      return webdav_session_close(WEBDAV_STATUS_NOT_FOUND);
    }

    webdav_session_set_status(WEBDAV_STATUS_CREATED);
    webdav_operation($target_collection, 'copy', $target_name, $source_collection);
    return webdav_session_close();
  }

  /**
   * PROPPATCH method handler
   *
   * @param  array  general parameter passing array
   * @return bool   true on success
   */
  function PROPPATCH(& $options) {
    webdav_session_start($options['path']);
    webdav_status_not_implemented();
    if (webdav_session_failed()) return webdav_session_close();
  }

  /**
   * LOCK method handler
   *
   * @param  array  general parameter passing array
   * @return bool   true on success
   */
  function LOCK(& $options) {
    webdav_session_start($options['path']);
    if (webdav_session_failed()) return webdav_session_status();

    // lookup the collection to lock
    $handler   = webdav_route_resolve(webdav_session('route'));
    if (!webdav_check_collection_perms($handler)) return webdav_session_close();
    if ($handler===false) return webdav_session_close(WEBDAV_STATUS_NOT_FOUND);

    webdav_operation($handler, 'lock');
    if (webdav_session_failed()) return webdav_session_close();

    $options["timeout"] = time()+webdav_lock_timeout();

    if (isset($options["update"])) { // Lock Update
//i      $cursor = db_query("SELECT * FROM {webdav_locks} WHERE path = '%s' AND token = '%s'",
//i      $options[path],
//i      $options[update]);
      $cursor = db_query("SELECT * FROM {webdav_locks} WHERE path = :s1 AND token = :s2",
      array(':s1'=> $options[path],':s2'=> $options[update]));

/*
$cursor = db_select('webdav_locks', 'c')
    ->fields('c')
    ->condition('path', $options[path],'=')
    ->condition('token', $options[update],'=')
    ->execute()
    ->fetchAssoc();
*/
       
      $lock = db_fetch_array($cursor);
      if ($lock) {
        $lock['expires']=$options[timeout];
        $lock['modified']=time();
        drupal_write_record('dav_locks', $lock, 'token');
        $options['owner'] = $lock['owner'];
        $options['scope'] = $lock["exclusivelock"] ? "exclusive" : "shared";
        $options['type']  = $lock["exclusivelock"] ? "write"     : "read";
        return true;
      } else {
        return false;
      }
    }

    $lock=array(
    'token'		=> 		$options[locktoken],
    'path'		=> 			$options[path],
    'created' => 	time(),
    'modified' => time(),
    'owner'   => $options[owner],
    'expires' => $options[timeout],
    'exclusivelock'  => $options['scope'] === "exclusive" ? "1" : "0",
    );
    drupal_write_record('webdav_locks', $lock);

    return webdav_session_close();
  }

  /**
   * UNLOCK method handler
   *
   * @param  array  general parameter passing array
   * @return bool   true on success
   */
  function UNLOCK(& $options) {
    webdav_session_start($options['path']);
    if (webdav_session_failed()) return webdav_session_close();

    // lookup the collection to lock
    $handler   = webdav_route_resolve(webdav_session('route'));
    if (!webdav_check_collection_perms($handler)) return webdav_session_close();
    if ($handler===false) return webdav_session_close(WEBDAV_STATUS_NOT_FOUND);

    webdav_operation($session, $handler, 'unlock');
    if (webdav_session_failed()) return webdav_session_close();

//i    db_query("DELETE FROM {webdav_locks} where path='%s' and token='%s'",
//i    $options[path],
//i    $options[token]);
    db_query("DELETE FROM {webdav_locks} WHERE path = :s1 AND token = :s2",
    array(':s1'=> $options[path],':s2'=> $options[token]));

    return webdav_session_close(WEBDAV_STATUS_NO_CONTENT);
  }
}
